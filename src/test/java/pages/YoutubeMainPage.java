package pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.net.MalformedURLException;

public class YoutubeMainPage extends PageObject {

    @FindBy(id = "Search")
    private WebElement searchButton;

    @FindBy(id = "com.google.android.youtube:id/search_edit_text")
    private WebElement searchInput;

    public WebElement getSearchButton() {

        return searchButton;
    }

    public WebElement getSearchInput() {

        return searchInput;
    }

    public YoutubeMainPage(AndroidDriver appDriver) throws MalformedURLException {
        super(appDriver);
    }
}
