package pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.support.PageFactory;

import java.net.MalformedURLException;

abstract class PageObject {

    private AndroidDriver appDriver;

    protected PageObject(AndroidDriver appDriver) throws MalformedURLException {
        this.appDriver = appDriver;
        PageFactory.initElements(appDriver, this);
    }
}
