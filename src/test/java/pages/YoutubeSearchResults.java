package pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.net.MalformedURLException;

public class YoutubeSearchResults extends PageObject {

    @FindBy(id = "Navigate up")
    private WebElement navigateUp;

    public WebElement getNavigateUp() {

        return navigateUp;
    }

    public YoutubeSearchResults(AndroidDriver appDriver) throws MalformedURLException {

        super(appDriver);
    }
}
