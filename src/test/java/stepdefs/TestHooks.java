package stepdefs;

import appiumutils.AppiumServer;
import cucumber.api.java.After;
import cucumber.api.java.Before;

import java.net.MalformedURLException;


public class TestHooks extends AbstractStepDefs {

    private AppiumServer server = new AppiumServer();


    @Before
    public void setup() throws MalformedURLException {

        server.startServer();
        driverInit();
    }

    @After
    public void tearDown() {

        driver.quit();
        server.stopServer();
    }
}
