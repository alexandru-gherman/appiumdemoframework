package stepdefs;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.junit.Assert.assertTrue;

public class Stepdefs extends AbstractStepDefs {

    @Given("^user is on the main page$")
    public void userIsOnTheMainPage() {

      assertTrue(main.getSearchButton().isDisplayed());
    }

    @When("^user taps the \"([^\"]*)\" button$")
    public void userTapsTheButton(String buttonName)  {

        main.getSearchButton().click();
    }

    @And("^user inputs \"([^\"]*)\" in the search bar$")
    public void userInputsInTheSearchBar(String value)  {

        main.getSearchInput().sendKeys(value + "\n");
    }

    @Then("^the \"([^\"]*)\" page is displayed$")
    public void thePageIsDisplayed(String arg0) {

       assertTrue(results.getNavigateUp().isDisplayed());
       results.getNavigateUp().click();
    }
}
