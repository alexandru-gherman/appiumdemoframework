package stepdefs;

import appiumutils.AppiumDriverManager;
import io.appium.java_client.android.AndroidDriver;
import pages.YoutubeMainPage;
import pages.YoutubeSearchResults;

import java.net.MalformedURLException;

abstract class AbstractStepDefs {

    private static AppiumDriverManager driverManager = new AppiumDriverManager();
    static AndroidDriver driver;

    protected static void driverInit() throws MalformedURLException {

        driver = driverManager.createDriver();
    }


    YoutubeMainPage main;

    {
        try {
            main = new YoutubeMainPage(driver);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    YoutubeSearchResults results;

    {
        try {
            results = new YoutubeSearchResults(driver);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }


}
