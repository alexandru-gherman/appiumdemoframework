Feature: user is able to use the search function

  Background:
    Given user is on the main page

  Scenario Outline: user searches a video by name
    When user taps the "search" button
    And user inputs "<Text>" in the search bar
    And the "Search results" page is displayed
    And user taps the "Back" button
    Then the "Main" page is displayed
    Examples:
      | Text |
      | a     |